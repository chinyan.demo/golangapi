// @title Golang Gin Framework Swagger API Documentation
// @version 1.0
// @description Golang API Boilerplate
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:11111
// @BasePath /
// @schemes http
package main

import (
	server "ginapp/src"

	"ginapp/src/config"

	_ "ginapp/docs"

	"github.com/gin-gonic/gin"
)

func main() {
	conf := config.GetEnv()
	Server := &server.Server{
		Host:       conf.HOST,
		Port:       conf.PORT,
		ProjectEnv: conf.ENV,
		Engine:     gin.Default(),
	}
	Server.Setup()
	Server.Start()
}
