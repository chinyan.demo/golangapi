package interest_test

import (
	"encoding/json"
	server "ginapp/src"
	"ginapp/src/api/interest"
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	userCredential "ginapp/src/helper/test_user"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/assert.v1"
)

var (
	App       *gin.Engine
	Env       config.Configuration
	UserToken string
)

func TestMain(m *testing.M) {
	Env = config.GetTestEnv()
	ColorLog.Yellow("Env : ", Env)
	testServer := &server.Server{
		Host:       Env.HOST,
		Port:       Env.PORT,
		ProjectEnv: Env.ENV,
		Engine:     gin.Default(),
	}
	testServer.Setup()
	App = testServer.GetServerEngine()
	UserToken = userCredential.GetToken(App)
	ColorLog.Yellow("#############################################")
	ColorLog.Yellow("Test Interest Module")
	ColorLog.Yellow("#############################################")
	exitVal := m.Run()
	defer os.Exit(exitVal)
}

func Test_List_Interest(t *testing.T) {
	var interestResponse interest.ResInterestList
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/interest/list", nil)
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &interestResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, interestResponse.Status)
	assert.Equal(t, "List Interest Successfully", interestResponse.Message)
}
