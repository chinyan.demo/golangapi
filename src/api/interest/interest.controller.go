package interest

import (
	Res "ginapp/src/helper/response"
	repo "ginapp/src/repository"

	"github.com/gin-gonic/gin"
)

// list ... all interest list
// @Summary list all interest
// @Description list all interest
// @Tags Interest
// @Accept json
// @Produce json
// @Success 200 {object} ResInterestList
// @Router /interest/list [get]
func list(ctx *gin.Context) {
	interests, err := repo.InterestRepo.List()
	if err != nil {
		Res.InternalServerError("List Interest", err, ctx)
		return
	}
	Res.Success("List Interest Successfully", map[string]any{"interests": interests}, ctx)
}
