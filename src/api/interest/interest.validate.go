package interest

import "ginapp/src/model"

/*||*************************************||*/
/*||	Response Model					 ||*/
/*||*************************************||*/
type ResInterestList struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Interests []*model.Interest `json:"interests"`
	} `json:"data"`
}
