package interest

import (
	"github.com/gin-gonic/gin"
)

func Router(app *gin.Engine) {
	interestRouter := app.Group("/interest")
	interestRouter.GET("/list", list)

}
