package public_test

import (
	"bytes"
	"encoding/json"
	server "ginapp/src"
	"ginapp/src/api/public"
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	"ginapp/src/helper/cryptography"
	"ginapp/src/model"
	"ginapp/src/model/component"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var (
	App *gin.Engine
	Env config.Configuration
)

func TestMain(m *testing.M) {
	Env = config.GetTestEnv()
	ColorLog.Yellow("Env : ", Env)
	testServer := &server.Server{
		Host:       Env.HOST,
		Port:       Env.PORT,
		ProjectEnv: Env.ENV,
		Engine:     gin.Default(),
	}
	testServer.Setup()
	App = testServer.GetServerEngine()
	ColorLog.Yellow("#############################################")
	ColorLog.Yellow("Test PUBLIC Module")
	ColorLog.Yellow("#############################################")
	exitVal := m.Run()
	defer os.Exit(exitVal)
}

func Test_HealthCheck(t *testing.T) {
	var healthCheckResponse public.ResHealthCheck
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/", nil)
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &healthCheckResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, healthCheckResponse.Status)
	assert.Equal(t, "Server Up And Running", healthCheckResponse.Message)
}

func Test_Ping(t *testing.T) {
	var pingResponse public.ResPing
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/ping", nil)
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &pingResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, pingResponse.Status)
	assert.Equal(t, "PONG", pingResponse.Message)
}

func Test_Echo(t *testing.T) {
	var echoResponse public.ResEcho

	payload := &public.EchoMessage{
		Text: "Test Message",
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/echo", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &echoResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, echoResponse.Status)
	assert.Equal(t, payload.Text, echoResponse.Message)
}

func Test_Register(t *testing.T) {
	var registerResponse public.ResRegister

	payload := &public.RegisterPayload{
		Password: cryptography.Base64Encoding("somePassword@123!"),
		Profile: model.Profile{
			FirstName: "Steven",
			LastName:  "Foxx",
			Email:     "stevenfoxx@hotmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
		},
		InterestIDs: []string{
			"interests/5f206db4-6dc2-435c-a072-be84315a4a64",
			"interests/d30dddc3-047e-46d8-b4ba-47b56ff40af0",
		},
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/register", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &registerResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, registerResponse.Status)
	// assert.Equal(t, payload.Text, registerResponse.Message)
}

func Test_Login(t *testing.T) {
	var loginResponse public.ResLogin

	payload := &public.LoginPayload{
		Email:    "stevenfoxx@gmail.com",
		Password: cryptography.Base64Encoding("somePassword@123!"),
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &loginResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, loginResponse.Status)
	// assert.Equal(t, payload.Text, registerResponse.Message)
}
