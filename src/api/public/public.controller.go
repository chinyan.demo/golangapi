package public

import (
	"errors"
	"net/http"

	"ginapp/src/config"
	"ginapp/src/database/arango"
	ColorLog "ginapp/src/helper/colorlog"
	"ginapp/src/helper/cryptography"
	docHelper "ginapp/src/helper/document"
	Res "ginapp/src/helper/response"
	"ginapp/src/model"
	"ginapp/src/model/component"
	repo "ginapp/src/repository"

	ArangoDriver "github.com/arangodb/go-driver"

	"github.com/gin-gonic/gin"
)

// HealthCheck godoc
// @Summary health check
// @Schemes
// @Description do ping
// @Tags Root
// @Accept json
// @Produce json
// @Success 200 {object} ResHealthCheck
// @Router / [get]
func HealthCheck(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": "Server Up And Running",
	})
}

// Ping godoc
// @Summary ping endpoint
// @Schemes
// @Description do ping
// @Tags Root
// @Accept json
// @Produce json
// @Success 200 {object} ResPing
// @Router /ping [get]
func Ping(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": "PONG",
	})
}

// Echo ... echo endpoint return whatever you send
// @Summary echo endpoint return whatever you send
// @Description do return whatever you send
// @Tags Root
// @Param Payload body EchoMessage true "return whatever you send"
// @Accept json
// @Produce json
// @Success 200 {object} ResEcho
// @Router /echo [post]
func Echo(ctx *gin.Context) {
	payload, _ := ctx.Get("ValidatedPayload")
	echoMessage := payload.(*EchoMessage)
	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": echoMessage.Text,
	})
}

// Register ... register user account
// @Summary register user account
// @Description register user account
// @Tags Root
// @Param Payload body RegisterPayload true "register user account"
// @Accept json
// @Produce json
// @Success 200 {object} ResRegister
// @Router /register [post]
func Register(ctx *gin.Context) {
	payload, _ := ctx.Get("ValidatedPayload")
	registerPayload := payload.(*RegisterPayload)

	profile := registerPayload.Profile
	password := registerPayload.Password
	interestIDs := registerPayload.InterestIDs

	profiles, err := repo.ProfileRepo.GetProfileByEmail(profile.Email)
	if err != nil {
		Res.InternalServerError("fetch profile failed", err, ctx)
		return
	}
	if len(profiles) > 0 {
		Res.BadRequest("Email taken by some others", errors.New("Email taken by some others"), ctx)
		return
	}
	// ColorLog.Yellow("len(profiles) : ", len(profiles))
	for _, p := range profiles {
		ColorLog.Yellow("profiles : ", p)
	}

	salt, hashedPassword := cryptography.HashPasswordWithSalt(password)

	profile, err = docHelper.AddBaseField[model.Profile](profile, config.COLLECTION.PROFILE, "", "")
	if err != nil {
		Res.InternalServerError("Add baseFields to profile failed", err, ctx)
		return
	}
	account, err := docHelper.AddBaseField[model.Account](&model.Account{
		ProfileID: profile.Id,
		Email:     profile.Email,
		Password: component.Password{
			HashedPassword: hashedPassword,
			Salt:           salt,
		},
	}, config.COLLECTION.ACCOUNT, profile.Id, "")
	if err != nil {
		Res.InternalServerError("Add baseFields to account failed", err, ctx)
		return
	}

	// Query
	_, err = arango.UseTransection(ArangoDriver.TransactionCollections{
		Write: []string{config.COLLECTION.PROFILE, config.COLLECTION.ACCOUNT},
	}, func() (any, error) {
		err = repo.ProfileRepo.Insert(profile)
		if err != nil {
			return nil, err
		}
		err = repo.AccountRepo.Insert(account)
		if err != nil {
			return nil, err
		}
		for _, interestID := range interestIDs {
			interested, err := docHelper.AddBaseField[model.Interested_Edge](
				&model.Interested_Edge{
					From: profile.Id,
					To:   interestID,
				},
				config.EDGE.INTERESTED, profile.Id, "",
			)
			if err != nil {
				return nil, err
			}
			repo.InterestEdgeRepo.Insert(interested)

		}
		return nil, nil
	})
	if err != nil {
		Res.InternalServerError("Insert profile or accound failed", err, ctx)
		return
	}

	Res.Success("user registered successfully", profile, ctx)

}

// Login ... user login
// @Summary user login
// @Description user login
// @Tags Root
// @Param Payload body LoginPayload true "user login"
// @Accept json
// @Produce json
// @Success 200 {object} ResLogin
// @Router /register [post]
func Login(ctx *gin.Context) {
	payload, _ := ctx.Get("ValidatedPayload")
	loginPayload := payload.(*LoginPayload)

	email := loginPayload.Email
	password := loginPayload.Password

	accs, err := repo.AccountRepo.FetchByEmail(email)
	if err != nil {
		Res.InternalServerError("fetch account failed", err, ctx)
		return
	}
	if len(accs) == 0 {
		Res.BadRequest("Invalid Email", errors.New("Invalid Email"), ctx)
		return
	}

	acc := accs[0]
	valid, err := cryptography.CheckPasswordWithSalt(password, acc.Password.HashedPassword, acc.Password.Salt)
	if err != nil {
		Res.InternalServerError("Check Password failed", err, ctx)
		return
	}
	if !valid {
		Res.BadRequest("Invalid Password", err, ctx)
		return
	}

	profileInterface, err := repo.ProfileRepo.Fetch(acc.ProfileID, &model.Profile{})
	if err != nil {
		Res.InternalServerError("Fetch Profile failed", err, ctx)
		return
	}

	profile := profileInterface.(*model.Profile)

	friendsID, err := repo.FriendshipRepo.GetFriendsID(profile.Id)
	if err != nil {
		Res.InternalServerError("Fetch FriendsID failed", err, ctx)
		return
	}
	// ColorLog.Yellow("friendsID :::: ", friendsID)
	// ColorLog.Yellow("LENGTH :::: ", len(friendsID.([]any)))
	ColorLog.Magenta("JWT DATA :::: ", map[string]interface{}{
		"profile":   profile,
		"accountID": acc.Id,
		"friendsID": friendsID,
	})

	signature := config.GetEnv().JWT_SIGNATURE
	acceccToken, err := cryptography.GenerateJWT(signature, map[string]interface{}{
		"profile":   profile,
		"accountID": acc.Id,
		"friendsID": friendsID,
	})
	if err != nil {
		Res.InternalServerError("Generate Token Failed", err, ctx)
		return
	}

	Res.Success("Login Successfully", map[string]any{
		"profile":     profile,
		"accessToken": acceccToken,
	}, ctx)

}
