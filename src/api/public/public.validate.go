package public

import "ginapp/src/model"

type EchoMessage struct {
	Text string `json:"text" validate:"required"`
}

type RegisterPayload struct {
	Profile     model.Profile `json:"profile" validate:"required"`
	Password    string        `json:"password" validate:"required"`
	InterestIDs []string      `json:"interestIDs" validate:"required"`
}

type LoginPayload struct {
	Email    string `json:"email" validate:"required"`
	Password string `json:"password"`
}

/*||*************************************||*/
/*||	Response Model					 ||*/
/*||*************************************||*/

type ResHealthCheck struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type ResPing struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type ResEcho struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type ResRegister struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    model.Profile `json:"data"`
}

type ResLogin struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    struct {
		Profile     *model.Profile `json:"profile"`
		AccessToken string         `json:"accessToken"`
	} `json:"data"`
}
