package public

import (
	mw "ginapp/src/middleware"
	plv "ginapp/src/middleware/validator"

	"github.com/gin-gonic/gin"
)

func Router(app *gin.Engine) {
	publicRouter := app.Group("/")
	publicRouter.GET("/", HealthCheck)
	publicRouter.GET("/ping", mw.TestMw(), Ping)
	publicRouter.POST("/echo", plv.Validate(&EchoMessage{}), Echo)
	publicRouter.POST("/register", plv.Validate(&RegisterPayload{}), Register)
	publicRouter.POST("/login", plv.Validate(&LoginPayload{}), Login)

}
