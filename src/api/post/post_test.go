package post_test

import (
	"bytes"
	"encoding/json"
	server "ginapp/src"
	"ginapp/src/api/post"
	"ginapp/src/config"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	ColorLog "ginapp/src/helper/colorlog"

	userCredential "ginapp/src/helper/test_user"

	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/assert.v1"
)

var (
	App       *gin.Engine
	Env       config.Configuration
	UserToken string
)

func TestMain(m *testing.M) {
	Env = config.GetTestEnv()
	ColorLog.Yellow("Env : ", Env)
	testServer := &server.Server{
		Host:       Env.HOST,
		Port:       Env.PORT,
		ProjectEnv: Env.ENV,
		Engine:     gin.Default(),
	}
	testServer.Setup()
	App = testServer.GetServerEngine()
	UserToken = userCredential.GetToken(App)
	ColorLog.Yellow("#############################################")
	ColorLog.Yellow("Test Post Module")
	ColorLog.Yellow("#############################################")
	exitVal := m.Run()
	defer os.Exit(exitVal)
}

func Test_CreatePost(t *testing.T) {
	var createResponse post.CreateResponse

	payload := &post.CreateRequest{
		CreatorProfileID: config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		Text:             "Hello everyone I am new here.",
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/post/create", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &createResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, createResponse.Status)
}

func Test_ListPost(t *testing.T) {
	var listResponse post.ListResponse

	ProfileID := config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/post/list?profileID="+ProfileID, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &listResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, listResponse.Status)
}

func Test_Update(t *testing.T) {
	var updateResponse post.UpdateResponse

	payload := &post.UpdateRequest{
		UpdaterProfileID: config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		PostID:           config.COLLECTION.POST + "/87fbe3db-5afd-46af-9a6f-b829a216e784",
		Text:             "Lorem ipsum sit amet.",
	}
	buffer, _ := json.Marshal(payload)
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPut, "/post/update", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &updateResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, updateResponse.Status)
}
func Test_Delete(t *testing.T) {
	var deleteResponse post.DeleteResponse

	payload := &post.DeleteRequest{
		DeleterProfileID: config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		PostID:           config.COLLECTION.POST + "/8e941790-9b67-43e5-b680-40a5d96b2774",
	}
	buffer, _ := json.Marshal(payload)
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodDelete, "/post/delete", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &deleteResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, deleteResponse.Status)
}
