package post

import (
	"ginapp/src/model"
)

type CreateRequest struct {
	CreatorProfileID string `json:"creatorProfileID" validate:"required"`
	Text             string `json:"text" validate:"required"`
}

type ListRequest struct {
	ProfileID string `form:"profileID" validate:"required"`
}

type UpdateRequest struct {
	PostID           string `json:"postID" validate:"required"`
	Text             string `json:"text" validate:"required"`
	UpdaterProfileID string `json:"updaterProfileID" validate:"required"`
}

type DeleteRequest struct {
	DeleterProfileID string `json:"deleterProfileID" validate:"required"`
	PostID           string `json:"postID" validate:"required"`
}

/*||*************************************||*/
/*||	Response Model					 ||*/
/*||*************************************||*/

type CreateResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ListResponse struct {
	Status  int           `json:"status"`
	Message string        `json:"message"`
	Data    []*model.Post `json:"data"`
}

type UpdateResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type DeleteResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
