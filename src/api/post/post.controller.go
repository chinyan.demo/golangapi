package post

import (
	"errors"

	"ginapp/src/config"
	"ginapp/src/helper/datetime"
	docHelper "ginapp/src/helper/document"
	Res "ginapp/src/helper/response"
	"ginapp/src/model"
	repo "ginapp/src/repository"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

// CreatePost ... create post
// @Summary create post
// @Description create post
// @Tags Post
// @Param Payload body CreateRequest true "create post"
// @Accept json
// @Produce json
// @Success 200 {object} CreateResponse
// @Router /post/create [post]
func CreatePost(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*CreateRequest)
	creatorProfileID := Payload.CreatorProfileID
	text := Payload.Text
	profile := JwtData["profile"].(map[string]any)

	if creatorProfileID != profile["_id"] {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
		return
	}

	Post, err := docHelper.AddBaseField[*model.Post](&model.Post{
		Text:      text,
		IsDeleted: false,
	}, config.COLLECTION.POST, creatorProfileID, "")
	if err != nil {
		Res.InternalServerError("Add base Field to Post Failed", errors.New("Add base Field to Post Failed"), ctx)
		return
	}

	err = repo.PostRepo.Insert(Post)
	if err != nil {
		Res.InternalServerError("Add base Field to Post Failed", errors.New("Add base Field to Post Failed"), ctx)
		return
	}

	Res.Success("Post created Successfully", Post, ctx)

}

// ListPost ... list post
// @Summary list post
// @Description list post
// @Tags Post
// @Param Payload query ListRequest true "list post"
// @Accept json
// @Produce json
// @Success 200 {object} ListResponse
// @Router /post/list [get]
func ListPost(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*ListRequest)
	ProfileID := Payload.ProfileID
	profile := JwtData["profile"].(map[string]any)
	friendsID := JwtData["friendsID"]

	if ProfileID != profile["_id"] {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
		return
	}

	posts, err := repo.PostRepo.GetPosts(ProfileID, friendsID)
	if err != nil {
		Res.InternalServerError("List Post Failed", err, ctx)
		return
	}

	Res.Success("List Post Successfully", posts, ctx)
}

// UpdatePost ... update post
// @Summary update post
// @Description update post
// @Tags Post
// @Param Payload body UpdateRequest true "update post"
// @Accept json
// @Produce json
// @Success 200 {object} UpdateResponse
// @Router /post/update [put]
func UpdatePost(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*UpdateRequest)

	UpdaterProfileID := Payload.UpdaterProfileID
	profile := JwtData["profile"].(map[string]any)

	updateData := map[string]any{
		"text":      Payload.Text,
		"updatedAt": datetime.GetIso8601(),
		"updatedBy": UpdaterProfileID,
	}

	if UpdaterProfileID != profile["_id"] {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
		return
	}

	err := repo.PostRepo.Update(updateData, Payload.PostID)
	if err != nil {
		Res.InternalServerError("Update Post Failed", err, ctx)
		return
	}

	Res.Success("Update Post Successfully", updateData, ctx)

}

// DeletePost ... delete post
// @Summary delete post
// @Description delete post
// @Tags Post
// @Param Payload body DeleteRequest true "delete post"
// @Accept json
// @Produce json
// @Success 200 {object} DeleteResponse
// @Router /post/delete [delete]
func DeletePost(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*DeleteRequest)
	ProfileID := Payload.DeleterProfileID
	profile := JwtData["profile"].(map[string]any)

	updateData := map[string]any{
		"isDeleted": true,
		"updatedAt": datetime.GetIso8601(),
		"updatedBy": ProfileID,
	}

	if ProfileID != profile["_id"] {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
		return
	}

	err := repo.PostRepo.Update(updateData, Payload.PostID)
	if err != nil {
		Res.InternalServerError("Delete Post Failed", err, ctx)
		return
	}

	Res.Success("Delete Post Successfully", updateData, ctx)
}
