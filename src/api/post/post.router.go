package post

import (
	"ginapp/src/middleware/jwt"
	v "ginapp/src/middleware/validator"

	"github.com/gin-gonic/gin"
)

func Router(app *gin.Engine) {
	postRouter := app.Group("/post")
	postRouter.Use(jwt.Validate())
	postRouter.POST("/create", v.Validate(&CreateRequest{}), CreatePost)
	postRouter.GET("/list", v.Validate(&ListRequest{}), ListPost)
	postRouter.PUT("/update", v.Validate(&UpdateRequest{}), UpdatePost)
	postRouter.DELETE("/delete", v.Validate(&DeleteRequest{}), DeletePost)
}
