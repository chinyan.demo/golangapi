package api

import (
	"ginapp/src/api/friend"
	"ginapp/src/api/interest"
	"ginapp/src/api/post"
	"ginapp/src/api/public"

	"github.com/gin-gonic/gin"
)

func ApiRouters(app *gin.Engine) {
	public.Router(app)
	interest.Router(app)
	friend.Router(app)
	post.Router(app)
}
