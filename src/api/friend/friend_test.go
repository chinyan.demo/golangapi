package friend_test

import (
	"bytes"
	"encoding/json"
	server "ginapp/src"
	"ginapp/src/api/friend"
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	userCredential "ginapp/src/helper/test_user"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/assert.v1"
)

var (
	App       *gin.Engine
	Env       config.Configuration
	UserToken string
)

func TestMain(m *testing.M) {
	Env = config.GetTestEnv()
	ColorLog.Yellow("Env : ", Env)
	testServer := &server.Server{
		Host:       Env.HOST,
		Port:       Env.PORT,
		ProjectEnv: Env.ENV,
		Engine:     gin.Default(),
	}
	testServer.Setup()
	App = testServer.GetServerEngine()
	UserToken = userCredential.GetToken(App)
	ColorLog.Yellow("#############################################")
	ColorLog.Yellow("Test Friend Module")
	ColorLog.Yellow("#############################################")
	exitVal := m.Run()
	defer os.Exit(exitVal)
}

func Test_AcceptRequest(t *testing.T) {
	var approveResponse friend.ApproveResponse

	payload := &friend.ApproveRequest{
		FriendshipEdgeID: config.EDGE.FRIENDSHIP + "/1732d566-b1ca-4125-99f5-cbab7d9641f0",
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPut, "/friend/request/approve", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &approveResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, approveResponse.Status)
}

func Test_CreateFriend(t *testing.T) {
	var createResponse friend.CreateResponse

	payload := &friend.CreateRequest{
		ProfileID: config.COLLECTION.PROFILE + "/e0b47280-2110-4a11-8fff-6863cdbef07e",
	}
	buffer, _ := json.Marshal(payload)

	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "/friend/request/create", bytes.NewBuffer(buffer))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &createResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, createResponse.Status)
}

func Test_ListFriendRequest(t *testing.T) {
	var listResponse friend.ListResponse

	ProfileID := config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/friend/request/list?profileID="+ProfileID, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &listResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, listResponse.Status)
}

func Test_ListFriendRecommendation(t *testing.T) {
	var listRecommendationResponse friend.RecommendationResponse

	ProfileID := config.COLLECTION.PROFILE + "/6cb2e861-502e-4327-8769-a2dbbb6e33e5"
	rec := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodGet, "/friend/recommendation/list?profileID="+ProfileID, nil)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+UserToken)
	App.ServeHTTP(rec, req)

	res := rec.Result()
	body, _ := ioutil.ReadAll(res.Body)
	jsonStr := string(body)
	if res.StatusCode == http.StatusOK {
		ColorLog.Green(jsonStr)
	} else {
		ColorLog.Red(jsonStr)
	}

	err := json.Unmarshal([]byte(jsonStr), &listRecommendationResponse)
	if err != nil {
		ColorLog.Red(err.Error())
		return
	}

	assert.Equal(t, 200, rec.Code)
	assert.Equal(t, 200, listRecommendationResponse.Status)
}
