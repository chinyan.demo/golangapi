package friend

import (
	jwt "ginapp/src/middleware/jwt"
	v "ginapp/src/middleware/validator"

	"github.com/gin-gonic/gin"
)

func Router(app *gin.Engine) {
	router := app.Group("/friend")
	router.Use(jwt.Validate())
	router.GET("/recommendation/list", v.Validate(&RecomendationListRequest{}), Recommendation)
	router.POST("/request/create", v.Validate(&CreateRequest{}), CreateFriendRequest)
	router.PUT("/request/approve", v.Validate(&ApproveRequest{}), ApproveFriendRequest)
	router.GET("/request/list", v.Validate(&ListRequest{}), ListFriendRequest)

}
