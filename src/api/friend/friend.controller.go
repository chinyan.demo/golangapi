package friend

import (
	"errors"
	"ginapp/src/config"
	docHelper "ginapp/src/helper/document"
	Res "ginapp/src/helper/response"
	"ginapp/src/model"
	repo "ginapp/src/repository"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

// Recommendation ... New Friend Recommendation
// @Summary New Friend Recommendation
// @Description New Friend Recommendation
// @Tags Friend
// @Param Payload query RecomendationListRequest true "New Friend Recommendation"
// @Accept json
// @Produce json
// @Success 200 {object} RecommendationResponse
// @Router /friend/recommendation/list [get]
func Recommendation(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)
	JwtProfile := JwtData["profile"].(map[string]any)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*RecomendationListRequest)

	profileID := Payload.ProfileID
	friendsID := JwtData["friendsID"]

	if JwtProfile["_id"] != profileID {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
	}

	profiles, err := repo.InterestEdgeRepo.Recommendation(profileID, friendsID)
	if err != nil {
		Res.InternalServerError("list recommendation failed", err, ctx)
		return
	}

	Res.Success("Friend Recommendation Listed Successfully", profiles, ctx)

}

// CreateFriendRequest ... Create Friend Request
// @Summary Create Friend Request
// @Description Create Friend Request
// @Tags Friend
// @Param Payload body CreateRequest true "Create Friend Request "
// @Accept json
// @Produce json
// @Success 200 {object} CreateResponse
// @Router /friend/request/create [post]
func CreateFriendRequest(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*CreateRequest)
	friendProfileID := Payload.ProfileID
	profile := JwtData["profile"].(map[string]any)

	friendShipEdge, err := docHelper.AddBaseField[*model.Friendship_Edge](model.Friendship_Edge{
		From:     profile["_id"].(string),
		To:       friendProfileID,
		Accepted: false,
	}, config.EDGE.INTERESTED, profile["_id"].(string), "")
	if err != nil {
		Res.InternalServerError("add basefield to friendship edge error", err, ctx)
		return
	}

	err = repo.FriendshipRepo.Insert(friendShipEdge)
	if err != nil {
		Res.InternalServerError("Insert friendship edge error", err, ctx)
		return
	}

	Res.Success("Friendship Created Successfully", map[string]any{}, ctx)

}

// ApproveFriendRequest ... Approve Friend Request
// @Summary Approve Friend Request
// @Description Approve Friend Request
// @Tags Friend
// @Param Payload body ApproveRequest true "Approve Friend Request"
// @Accept json
// @Produce json
// @Success 200 {object} ApproveResponse
// @Router /friend/request/approve [put]
func ApproveFriendRequest(ctx *gin.Context) {
	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*ApproveRequest)
	friendshipEdgeID := Payload.FriendshipEdgeID

	err := repo.FriendshipRepo.Update(map[string]any{"accepted": true}, friendshipEdgeID)
	if err != nil {
		Res.InternalServerError("update friendship edge error", err, ctx)
		return
	}

	Res.Success("Friendship Approved Successfully", map[string]any{"friendshipEdgeID": friendshipEdgeID}, ctx)

}

// ListFriendRequest ... list friend request
// @Summary list friend request
// @Description list friend request
// @Tags Friend
// @Param Payload query ListRequest true "list friend request"
// @Accept json
// @Produce json
// @Success 200 {object} ListResponse
// @Router /friend/request/list [get]
func ListFriendRequest(ctx *gin.Context) {
	jwtData, _ := ctx.Get("jwtData")
	JwtData := jwtData.(jwt.MapClaims)
	JwtProfile := JwtData["profile"].(map[string]any)

	payload, _ := ctx.Get("ValidatedPayload")
	Payload := payload.(*ListRequest)
	ProfileID := Payload.ProfileID

	if JwtProfile["_id"] != ProfileID {
		Res.BadRequest("Action Forbidden", errors.New("Unauthorized"), ctx)
	}

	friendRequestList, err := repo.FriendshipRepo.FriendRequestList(ProfileID)
	if err != nil {
		Res.InternalServerError("List Friend Request Error", err, ctx)
		return
	}

	Res.Success("List Friend Request Successfully", friendRequestList, ctx)
}
