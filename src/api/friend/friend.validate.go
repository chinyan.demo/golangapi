package friend

import (
	"ginapp/src/model"
	"ginapp/src/repository"
)

type RecomendationListRequest struct {
	ProfileID string `form:"profileID" validate:"required"`
}
type CreateRequest struct {
	ProfileID string `json:"profileID" validate:"required"`
}

type ListRequest struct {
	ProfileID string `form:"profileID" validate:"required"`
}

type ApproveRequest struct {
	FriendshipEdgeID string `json:"friendshipEdgeID" validate:"required"`
}

/*||*************************************||*/
/*||	Response Model					 ||*/
/*||*************************************||*/

type ApproveResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    struct {
		FriendshipEdgeID string `json:"friendshipEdgeID"`
	} `json:"data"`
}

type CreateResponse struct {
	Status  int         `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ListResponse struct {
	Status  int                                  `json:"status"`
	Message string                               `json:"message"`
	Data    []*repository.FriendRequestListModel `json:"data"`
}

type RecommendationResponse struct {
	Status  int              `json:"status"`
	Message string           `json:"message"`
	Data    []*model.Profile `json:"data"`
}
