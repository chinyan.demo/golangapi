package server

import (
	"ginapp/src/api"
	"ginapp/src/database/arango"
	"ginapp/src/middleware/swagger"

	"github.com/gin-gonic/gin"
)

type Server struct {
	Host       string
	Port       string
	ProjectEnv string
	Engine     *gin.Engine
}

func (s *Server) Setup() {
	app := s.Engine
	arango.Connect()
	api.ApiRouters(app)
	swagger.Swagger(app)
	arango.SetupArango()

}

func (s *Server) GetServerEngine() *gin.Engine {
	return s.Engine
}

func (s *Server) Start() {
	app := s.Engine
	app.Run(":" + s.Port)
}
