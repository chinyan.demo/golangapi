package validator

import (
	ColorLog "ginapp/src/helper/colorlog"
	Res "ginapp/src/helper/response"

	"github.com/gin-gonic/gin"
	v "github.com/go-playground/validator"
)

var PayloadValidator *v.Validate

func GetValidator() *v.Validate {
	if PayloadValidator == nil {
		PayloadValidator = v.New()
	}
	return PayloadValidator
}

func Validate(schema interface{}) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		if bindingErr := ctx.Bind(schema); bindingErr != nil {
			ColorLog.Red(bindingErr.Error())
			Res.InternalServerError(bindingErr.Error(), bindingErr, ctx)
			ctx.Abort()

		}

		payloadValidator := GetValidator()

		// ColorLog.Cyan("PayloadValidator Exist")
		if validationError := payloadValidator.Struct(schema); validationError != nil {
			ColorLog.Red(validationError.Error())
			Res.BadRequest(validationError.Error(), validationError, ctx)
			ctx.Abort()

		}

		// ColorLog.Yellow(schema)
		ctx.Set("ValidatedPayload", schema)
		ctx.Next()

	}
}
