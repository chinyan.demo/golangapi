package mw

import (
	ColorLog "ginapp/src/helper/colorlog"

	"github.com/gin-gonic/gin"
)

func TestMw() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		ColorLog.Magenta("Middleware Trigger")
	}
}
