package jwt

import (
	"errors"
	"fmt"
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	Res "ginapp/src/helper/response"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
)

var JWTSignature string = config.GetEnv().JWT_SIGNATURE

func Validate() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		authHeader := ctx.Request.Header["Authorization"][0]
		if len(authHeader) == 0 {
			err := "Authorization token required"
			Res.Unauthorized(err, errors.New(err), ctx)
			ctx.Abort()
		}

		authHeaderSplit := strings.Split(authHeader, " ")
		if len(authHeaderSplit) < 2 {
			err := "Invalid token format"
			Res.Unauthorized(err, errors.New(err), ctx)
			ctx.Abort()
		}
		ColorLog.Yellow("authHeaderSplit : ok")

		authType := authHeaderSplit[0]
		ColorLog.Yellow("authType : ok ")

		token := authHeaderSplit[1]
		if strings.ToLower(authType) != "bearer" {
			err := "Auth type is not Bearer"
			Res.Unauthorized(err, errors.New(err), ctx)
			ctx.Abort()
		}
		if len(token) == 0 {
			err := "Token is required"
			Res.Unauthorized(err, errors.New(err), ctx)
			ctx.Abort()
		}
		ColorLog.Yellow("token : ok ")

		var mySigningKey = []byte(JWTSignature)

		jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("There was an error in parsing")
			}
			return mySigningKey, nil
		})

		if err != nil {
			err := "Token Malform"
			Res.BadRequest(err, errors.New(err), ctx)
			ctx.Abort()
		}

		if jwtData, ok := jwtToken.Claims.(jwt.MapClaims); ok && jwtToken.Valid {
			ColorLog.Blue("JWT Data : ok")
			ColorLog.Blue(jwtData)

			//Push data to ctx
			ctx.Set("jwtData", jwtData)

			//pass ctx to next node
			ctx.Next()

		}
	}
}
