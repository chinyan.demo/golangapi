package model

import (
	"ginapp/src/model/component"
)

type Profile struct {
	Id        string            `json:"_id" validate:"omitempty"`
	Key       string            `json:"_key" validate:"omitempty"`
	FirstName string            `json:"firstName" validate:"required"`
	LastName  string            `json:"lastName" validate:"required"`
	Email     string            `json:"email" validate:"required"`
	Contact   component.Contact `json:"contact" validate:"required"`
	Address   component.Address `json:"address" validate:"required"`
	CreatedAt string            `json:"createdAt" validate:"omitempty"`
	CreatedBy string            `json:"createdBy" validate:"omitempty"`
	UpdatedAt string            `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string            `json:"updatedBy" validate:"omitempty"`
}
