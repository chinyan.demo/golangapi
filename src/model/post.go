package model

type Post struct {
	Id        string `json:"_id" validate:"omitempty"`
	Key       string `json:"_key" validate:"omitempty"`
	Text      string `json:"text" valiate:"required"`
	IsDeleted bool   `json:"isDeleted" validate:"omitempty"`
	CreatedAt string `json:"createdAt" validate:"omitempty"`
	CreatedBy string `json:"createdBy" validate:"omitempty"`
	UpdatedAt string `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string `json:"updatedBy" validate:"omitempty"`
}
