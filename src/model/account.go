package model

import (
	"ginapp/src/model/component"
)

type Account struct {
	Id        string             `json:"_id" validate:"omitempty"`
	Key       string             `json:"_key" validate:"omitempty"`
	ProfileID string             `json:"profileID"`
	Email     string             `json:"email"`
	Password  component.Password `json:"password"`
	CreatedAt string             `json:"createdAt" validate:"omitempty"`
	CreatedBy string             `json:"createdBy" validate:"omitempty"`
	UpdatedAt string             `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string             `json:"updatedBy" validate:"omitempty"`
}
