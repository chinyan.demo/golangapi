package model

type Interest_Category_Edge struct {
	Id        string `json:"_id" validate:"omitempty"`
	Key       string `json:"_key" validate:"omitempty"`
	From      string `json:"_from" validate:"omitempty"`
	To        string `json:"_to" validate:"omitempty"`
	CreatedAt string `json:"createdAt" validate:"omitempty"`
	CreatedBy string `json:"createdBy" validate:"omitempty"`
	UpdatedAt string `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string `json:"updatedBy" validate:"omitempty"`
}
