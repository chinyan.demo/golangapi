package model

type Interest struct {
	Id        string `json:"_id" validate:"omitempty"`
	Key       string `json:"_key" validate:"omitempty"`
	Name      string `json:"name" validate:"omitempty"`
	CreatedAt string `json:"createdAt" validate:"omitempty"`
	CreatedBy string `json:"createdBy" validate:"omitempty"`
	UpdatedAt string `json:"updatedAt" validate:"omitempty"`
	UpdatedBy string `json:"updatedBy" validate:"omitempty"`
}
