package component

type Address struct {
	Street1    string `json:"street1" validate:"required"`
	Street2    string `json:"street2" validate:"omitempty"`
	Town       string `json:"town" validate:"required"`
	PostalCode int    `json:"postalCode" validate:"required"`
	City       string `json:"city" validate:"required"`
	State      string `json:"state" validate:"required"`
}

type Contact struct {
	CountryCode string `json:"countryCode" validate:"required"`
	PhoneNumber string `json:"phoneNumber" validate:"required"`
	FullNumber  string `json:"fullNumber" validate:"required"`
}
