package component

type Password struct {
	HashedPassword string `json:"hashedPassword"`
	Salt           string `json:"salt"`
}
