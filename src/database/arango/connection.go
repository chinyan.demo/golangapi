package arango

import (
	ColorLog "ginapp/src/helper/colorlog"

	"ginapp/src/config"

	"github.com/arangodb/go-driver"
	ArangoDriver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
)

//Environment Variable
var Env config.Configuration = config.GetEnv()

// Instances
var arangoClient ArangoDriver.Client
var arangoConnection ArangoDriver.Connection
var arangoDB ArangoDriver.Database

func Connect() error {
	_, err := GetConnection()
	if err != nil {
		return err
	}
	_, err = GetClient()
	if err != nil {
		return err
	}

	_, err = GetDatabase()
	if err != nil {
		return err
	} else {
		ColorLog.Green("Connected To ArangoDB : ", Env.ARANGODB_DBNAME)
	}

	return nil
}

//Singleton Functions
func GetConnection() (ArangoDriver.Connection, error) {

	if arangoConnection == nil {
		connectionString := Env.ARANGODB_PROTOCOL + "://" + Env.ARANGODB_HOST + ":" + Env.ARANGODB_PORT + "/"
		ColorLog.Cyan("connectionString : ", connectionString)
		conn, err := http.NewConnection(http.ConnectionConfig{
			Endpoints: []string{connectionString},
		})
		if err != nil {
			ColorLog.Red("Arango Connection Error : ", err)
			return nil, err
		}
		arangoConnection = conn
	}
	return arangoConnection, nil
}

func GetClient() (ArangoDriver.Client, error) {
	// ColorLog.Cyan("Getting Arango Client ...")
	if arangoClient == nil {
		// ColorLog.Magenta(Env)
		// ColorLog.Magenta(map[string]any{"User": Env.ARANGODB_USER, "Password": Env.ARANGODB_PASSWORD})
		conn, _ := GetConnection()
		client, err := ArangoDriver.NewClient(ArangoDriver.ClientConfig{
			Connection:     conn,
			Authentication: driver.BasicAuthentication(Env.ARANGODB_USER, Env.ARANGODB_PASSWORD),
		})
		if err != nil {
			ColorLog.Red("Get Arango Client Error : ", err)
			return nil, err
		}
		arangoClient = client
	}
	return arangoClient, nil
}

func GetDatabase() (ArangoDriver.Database, error) {
	client, err := GetClient()
	if err != nil {
		return nil, err
	}

	if arangoDB == nil {
		db_exists, _ := client.DatabaseExists(nil, Env.ARANGODB_DBNAME)
		if db_exists {
			ColorLog.Blue("No db is created, " + Env.ARANGODB_DBNAME + " exists already")

			db, err := client.Database(nil, Env.ARANGODB_DBNAME)
			if err != nil {
				ColorLog.Red("Failed to open existing database:", err)
				return nil, err
			}
			arangoDB = db
		} else {
			ColorLog.Cyan("Creating Arango DB ...")
			db, err := client.CreateDatabase(nil, Env.ARANGODB_DBNAME, nil)
			if err != nil {
				ColorLog.Red("Failed to create database :", err)
				return nil, err
			} else {
				ColorLog.Green(Env.ARANGODB_DBNAME + " Created Successfully")
			}
			arangoDB = db
		}
	}
	return arangoDB, nil
}
