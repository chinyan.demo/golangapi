package arango

import (
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	docHelper "ginapp/src/helper/document"
)

func ResetDB() {
	Env := config.GetEnv()
	if Env.ENV == "test" {
		db, err := GetDatabase()
		if err != nil {
			ColorLog.Red("GET DB ERROR :", err)
		}

		for _, val := range docHelper.DocToMap(config.COLLECTION) {

			collection, err := db.Collection(nil, val.(string))
			if err != nil {
				ColorLog.Red("Collection : " + val.(string))
				ColorLog.Red("GET COLLECTION ERROR : ", err)
			}
			truncateError := collection.Truncate(nil)
			if truncateError != nil {
				ColorLog.Red(truncateError.Error())
				continue
			}
			ColorLog.Magenta("Truncated " + val.(string) + " Collection In " + Env.ARANGODB_DBNAME + " Database")
		}

		for _, edg := range docHelper.DocToMap(config.EDGE) {

			edgeCollection, err := db.Collection(nil, edg.(string))
			if err != nil {
				ColorLog.Red("Collection : " + edg.(string))
				ColorLog.Red("GET COLLECTION ERROR :", err)
			}
			truncateError := edgeCollection.Truncate(nil)
			if truncateError != nil {
				ColorLog.Red(truncateError.Error())
				continue
			}
			ColorLog.Magenta("Truncated " + edg.(string) + " EDGE Collection In " + Env.ARANGODB_DBNAME + " Database")
		}
	}
}
