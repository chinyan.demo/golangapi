package arango

import (
	"ginapp/src/config"
	testSeed "ginapp/src/database/arango/seed/test"
	ColorLog "ginapp/src/helper/colorlog"
	docHelper "ginapp/src/helper/document"
	"regexp"
)

func Seed() {
	Env := config.GetEnv().ENV
	db, _ := GetDatabase()
	if Env == "test" {
		for _, seedMatrices := range testSeed.TestSeedMatrices {
			for _, doc := range seedMatrices["Data"].([]any) {

				collection := seedMatrices["Collection"]
				mapDoc := docHelper.DocToMap(doc)
				uuidV4Regex := "(?i)[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}"
				key := mapDoc["_key"].(string)
				id := mapDoc["_id"].(string)

				if len(key) == 0 {
					ColorLog.Red("_key is required. Collection : ", collection)
					continue
				}
				matchKey, _ := regexp.MatchString(uuidV4Regex, key)
				if matchKey == false {
					ColorLog.Red("_key is required to be uuidV4.  Collection : ", collection)
					continue
				}

				query := `
					LET found = Document(@Id)
					RETURN (LENGTH(found) == 0 )? (
						FILTER LENGTH(found) == 0
						INSERT @data INTO @@collection
						RETURN "DATA INSERTED"
				   )[0] : "DATA EXIST"
				`

				bindVars := map[string]any{
					"Id":          id,
					"data":        doc,
					"@collection": collection,
				}

				// crusor, _ := db.Query(nil, query, bindVars)

				crusor, err := db.Query(nil, query, bindVars)
				if err != nil {
					// ColorLog.Red("Seed Query Malform : ", err)
					continue
				}
				defer crusor.Close()
			}
		}
	}
}
