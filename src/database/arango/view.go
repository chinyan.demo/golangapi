package arango

import (
	ViewMatrices "ginapp/src/database/arango/seed/view"

	ColorLog "ginapp/src/helper/colorlog"
)

func SetupView() {
	db, err := GetDatabase()
	if err != nil {
		ColorLog.Red(err)
	}

	for _, viewMatrix := range ViewMatrices.ViewMatrices {
		viewExist, err := db.ViewExists(nil, viewMatrix.Name)
		if err != nil {
			ColorLog.Red("Error Check View : ", err)
		}
		if !viewExist {
			arangoSearchView, err := db.CreateArangoSearchView(nil, viewMatrix.Name, viewMatrix.View)
			if err == nil {
				ColorLog.Red("Error Create View : ", err)
			}
			ColorLog.Green("Arango Search View ::: ", arangoSearchView)
			ColorLog.Green(viewMatrix.Name + " created. ")
			dv, _ := db.Views(nil)
			ColorLog.Yellow(dv)
		}
	}

}
