package ViewMatrices

import (
	"ginapp/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

type AliesViewMatrix struct {
	Name      string
	AliesView *ArangoDriver.ArangoSearchAliasViewProperties
}

type ViewMatrix struct {
	Name string
	View *ArangoDriver.ArangoSearchViewProperties
}

var coll = config.COLLECTION

var AliesViewMatrices = []AliesViewMatrix{
	*PostAliasView,
}

var ViewMatrices = []ViewMatrix{
	*PostView,
}
