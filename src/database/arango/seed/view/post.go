package ViewMatrices

import (
	ArangoDriver "github.com/arangodb/go-driver"
)

var PostAliasView = &AliesViewMatrix{
	Name: "post_view",
	AliesView: &ArangoDriver.ArangoSearchAliasViewProperties{
		Indexes: []ArangoDriver.ArangoSearchAliasIndex{
			{
				Collection: coll.POST,
				Index:      "createdBy",
			},
		},
	},
}

var PostView = &ViewMatrix{
	Name: "posts_view",
	View: &ArangoDriver.ArangoSearchViewProperties{
		Links: ArangoDriver.ArangoSearchLinks{
			coll.POST: ArangoDriver.ArangoSearchElementProperties{
				Analyzers: []string{"identity", "text_en"},
				Fields: ArangoDriver.ArangoSearchFields{
					"_id":       ArangoDriver.ArangoSearchElementProperties{},
					"_key":      ArangoDriver.ArangoSearchElementProperties{},
					"text":      ArangoDriver.ArangoSearchElementProperties{},
					"isDeleted": ArangoDriver.ArangoSearchElementProperties{},
					"createdAt": ArangoDriver.ArangoSearchElementProperties{},
					"createdBy": ArangoDriver.ArangoSearchElementProperties{},
					"updatedAt": ArangoDriver.ArangoSearchElementProperties{},
					"updatedBy": ArangoDriver.ArangoSearchElementProperties{},
				},
			},
		},
		//PrimarySortCompression: "LZ4",
	},
}
