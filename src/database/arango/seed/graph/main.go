package GraphSeedMatrices

import (
	"ginapp/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

type GraphSeedMatric struct {
	GraphName      string
	GraphAttribute *ArangoDriver.CreateGraphOptions
}

var coll = config.COLLECTION
var edge = config.EDGE

var GraphSeedMatrices = []GraphSeedMatric{
	*FriendshipGraph,
	*HobbiesGraph,
}
