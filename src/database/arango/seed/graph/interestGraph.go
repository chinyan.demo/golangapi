package GraphSeedMatrices

import (
	ArangoDriver "github.com/arangodb/go-driver"
)

var HobbiesGraph = &GraphSeedMatric{
	GraphName: "interest_graph",
	GraphAttribute: &ArangoDriver.CreateGraphOptions{
		EdgeDefinitions: []ArangoDriver.EdgeDefinition{
			{
				Collection: edge.INTERESTED,
				To:         []string{coll.INTEREST},
				From:       []string{coll.PROFILE},
			},
			{
				Collection: edge.INTEREST_CATEGORY,
				To:         []string{coll.INTEREST_CATEGORY},
				From:       []string{coll.INTEREST},
			},
		},
	},
}
