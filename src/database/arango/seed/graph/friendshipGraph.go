package GraphSeedMatrices

import (
	ArangoDriver "github.com/arangodb/go-driver"
)

var FriendshipGraph = &GraphSeedMatric{
	GraphName: "friendship_graph",
	GraphAttribute: &ArangoDriver.CreateGraphOptions{
		EdgeDefinitions: []ArangoDriver.EdgeDefinition{
			{
				Collection: edge.FRIENDSHIP,
				To:         []string{coll.PROFILE},
				From:       []string{coll.PROFILE},
			},
		},
	},
}
