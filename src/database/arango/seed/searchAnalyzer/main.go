package SearchAnylizerSeedMatrices

import (
	"ginapp/src/config"

	ArangoDriver "github.com/arangodb/go-driver"
)

var coll = config.COLLECTION

var SearchAnylizerSeedMatrices = []*ArangoDriver.ArangoSearchAnalyzerDefinition{}
