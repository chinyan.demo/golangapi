package testSeed

import (
	testData "ginapp/src/database/arango/seed/test/data"
)

var TestSeedMatrices = []map[string]any{
	testData.AccountSeedMatrices,
	testData.ProfileSeedMatrices,
	testData.PostSeedMatrices,
	testData.InterestCategorySeedMatrices,
	testData.InterestSeedMatrices,
	testData.InterestCategoryEdgeSeedMatrices,

	testData.InterestedEdgeSeedMatrices,
	testData.FriendshipEdgeSeedMatrices,
}
