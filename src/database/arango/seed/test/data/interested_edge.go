package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
)

var InterestedEdgeSeedMatrices = map[string]any{
	"Collection": config.EDGE.INTERESTED,
	"Seed":       true,
	"Data": []interface{}{
		&model.Interested_Edge{
			Id:        config.EDGE.INTERESTED + "/9973ce54-e028-4b3b-8cdf-542bbf58d683",
			Key:       "9973ce54-e028-4b3b-8cdf-542bbf58d683",
			From:      "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			To:        "interests/5f206db4-6dc2-435c-a072-be84315a4a64",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interested_Edge{
			Id:        config.EDGE.INTERESTED + "/b70d252b-87aa-4637-9958-9b6d85183a68",
			Key:       "b70d252b-87aa-4637-9958-9b6d85183a68",
			From:      "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			To:        "interests/d30dddc3-047e-46d8-b4ba-47b56ff40af0",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interested_Edge{
			Id:        config.EDGE.INTERESTED + "/bc39e63b-a4ba-412b-8401-02d950cfd906",
			Key:       "bc39e63b-a4ba-412b-8401-02d950cfd906",
			From:      "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			To:        "interests/5f206db4-6dc2-435c-a072-be84315a4a64",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
		},
		&model.Interested_Edge{
			Id:        config.EDGE.INTERESTED + "/d4e49df7-4b1d-4c63-bf2a-9bd1c48b8f26",
			Key:       "d4e49df7-4b1d-4c63-bf2a-9bd1c48b8f26",
			From:      "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			To:        "interests/d30dddc3-047e-46d8-b4ba-47b56ff40af0",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
		},
	},
}
