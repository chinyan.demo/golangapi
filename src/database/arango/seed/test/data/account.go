package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
	"ginapp/src/model/component"
)

var AccountSeedMatrices = map[string]any{
	"Collection": config.COLLECTION.ACCOUNT,
	"Seed":       true,
	"Data": []interface{}{
		&model.Account{
			Id:        "accounts/bb61949c-1bc8-4657-b402-f71f109f947c",
			Key:       "bb61949c-1bc8-4657-b402-f71f109f947c",
			ProfileID: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			Email:     "stevenfoxx@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		},
		&model.Account{
			Id:        "accounts/1b412e3b-13ec-4ab1-9bff-cb1237faf6f1",
			Key:       "1b412e3b-13ec-4ab1-9bff-cb1237faf6f1",
			ProfileID: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			Email:     "meganfoxx@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
		},
		&model.Account{
			Id:        "accounts/329cb0f5-01ef-4620-97b5-fe476e229a28",
			Key:       "329cb0f5-01ef-4620-97b5-fe476e229a28",
			ProfileID: "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
			Email:     "bradpitt@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
		},
		&model.Account{
			Id:        "accounts/a9163ffe-27f6-45c6-bcf9-4481b0f0d4ae",
			Key:       "a9163ffe-27f6-45c6-bcf9-4481b0f0d4ae",
			ProfileID: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			Email:     "jenniferlopez@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
		},
		&model.Account{
			Id:        "accounts/3efdee81-65b4-485a-a3df-572af1bd3843",
			Key:       "3efdee81-65b4-485a-a3df-572af1bd3843",
			ProfileID: "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			Email:     "johnnydepp@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
		},
		&model.Account{
			Id:        "accounts/ab3734f0-5950-4203-bf81-24818d1c0f2d",
			Key:       "ab3734f0-5950-4203-bf81-24818d1c0f2d",
			ProfileID: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			Email:     "tayloeswift@gmail.com",
			Password: component.Password{
				HashedPassword: "$2a$10$PkZJUbXFmur66VJgi1dIHOKBvJruRbyYk5HzTTqh9QpdDJjt8fGPi",
				Salt:           "7006791947779410",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
		},
	},
}
