package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
)

var InterestCategorySeedMatrices = map[string]any{
	"Collection": config.COLLECTION.INTEREST_CATEGORY,
	"Seed":       true,
	"Data": []interface{}{
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/388d62df-aec4-444f-8e8c-e8d6bb9099ed",
			Key:       "388d62df-aec4-444f-8e8c-e8d6bb9099ed",
			Name:      "Travel",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/af278256-0a5a-4a28-a69e-e86c917605a0",
			Key:       "af278256-0a5a-4a28-a69e-e86c917605a0",
			Name:      "Fine Art",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/a6e738f6-7247-46fc-be36-836f60c8b3cc",
			Key:       "a6e738f6-7247-46fc-be36-836f60c8b3cc",
			Name:      "Music",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/63a551b9-13c0-4db8-8585-084b49255ecd",
			Key:       "63a551b9-13c0-4db8-8585-084b49255ecd",
			Name:      "Sport",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/2ee40801-9dd5-4d35-b621-0bdd66b6467b",
			Key:       "2ee40801-9dd5-4d35-b621-0bdd66b6467b",
			Name:      "Literature",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/9f2acbf3-b96a-4aba-af02-9dcd31bb1335",
			Key:       "9f2acbf3-b96a-4aba-af02-9dcd31bb1335",
			Name:      "Games",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/1ab3a636-23f8-4d03-a191-d7e0c2ea4ec1",
			Key:       "1ab3a636-23f8-4d03-a191-d7e0c2ea4ec1",
			Name:      "Technology",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/6297668e-be43-4375-8835-86886cc1b174",
			Key:       "6297668e-be43-4375-8835-86886cc1b174",
			Name:      "Drama and Dance",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
		&model.Interest_Category{
			Id:        config.COLLECTION.INTEREST_CATEGORY + "/1273954b-0b89-4a6a-9464-f63b05fdfcec",
			Key:       "1273954b-0b89-4a6a-9464-f63b05fdfcec",
			Name:      "Pets",
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/313e5df9-cae1-41bb-b164-97b7098dd517",
		},
	},
}
