package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
	"ginapp/src/model/component"
)

var ProfileSeedMatrices = map[string]any{
	"Collection": config.COLLECTION.PROFILE,
	"Seed":       true,
	"Data": []interface{}{
		&model.Profile{
			Id:        "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			Key:       "6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			FirstName: "Steven",
			LastName:  "Foxx",
			Email:     "stevenfoxx@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		},
		&model.Profile{
			Id:        "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			Key:       "e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			FirstName: "Megan",
			LastName:  "Foxx",
			Email:     "meganfoxx@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
		},

		&model.Profile{
			Id:        "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
			Key:       "e0b47280-2110-4a11-8fff-6863cdbef07e",
			FirstName: "brad",
			LastName:  "pitt",
			Email:     "bradpitt@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/e0b47280-2110-4a11-8fff-6863cdbef07e",
		},

		&model.Profile{
			Id:        "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			Key:       "92106aef-9443-40be-8c8d-17edb92f37dc",
			FirstName: "jennifer",
			LastName:  "lopez",
			Email:     "jenniferlopez@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
		},

		&model.Profile{
			Id:        "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			Key:       "b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			FirstName: "johnny",
			LastName:  "depp",
			Email:     "johnydepp@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
		},

		&model.Profile{
			Id:        "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			Key:       "c9dbd816-5602-4301-ab4a-266cc0933b2b",
			FirstName: "Taylor",
			LastName:  "Swift",
			Email:     "taylorswift@gmail.com",
			Contact: component.Contact{
				CountryCode: "+60",
				PhoneNumber: "0123456789",
				FullNumber:  "+60123456789",
			},
			Address: component.Address{
				Street1:    "Jalan Dato Onn",
				Street2:    "",
				Town:       "Kuala Lumpur",
				PostalCode: 50480,
				City:       "Wilayah Persekutuam Kuala Lumpur",
				State:      "Selangor",
			},
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/c9dbd816-5602-4301-ab4a-266cc0933b2b",
		},
	},
}
