package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
)

var FriendshipEdgeSeedMatrices = map[string]any{
	"Collection": config.EDGE.FRIENDSHIP,
	"Seed":       true,
	"Data": []any{
		&model.Friendship_Edge{
			Id:        config.EDGE.FRIENDSHIP + "/1732d566-b1ca-4125-99f5-cbab7d9641f0",
			Key:       "1732d566-b1ca-4125-99f5-cbab7d9641f0",
			From:      "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			To:        "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			Accepted:  false,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/e3d55a9c-fa53-42d1-a08c-071da3762bf5",
		},
		&model.Friendship_Edge{
			Id:        config.EDGE.FRIENDSHIP + "/a1dd7b8b-fa37-4e42-8246-850f7bd92bec",
			Key:       "a1dd7b8b-fa37-4e42-8246-850f7bd92bec",
			From:      "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			To:        "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			Accepted:  true,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
		},
		&model.Friendship_Edge{
			Id:        config.EDGE.FRIENDSHIP + "/d62f8c12-5672-4f65-8507-45700d738104",
			Key:       "d62f8c12-5672-4f65-8507-45700d738104",
			From:      "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			To:        "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			Accepted:  false,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/b444d1eb-6061-4822-b60a-ddc53c0f09ba",
		},
	},
}
