package testData

import (
	"ginapp/src/config"
	"ginapp/src/model"
)

var PostSeedMatrices = map[string]any{
	"Collection": config.COLLECTION.POST,
	"Seed":       true,
	"Data": []any{
		&model.Post{
			Id:        config.COLLECTION.POST + "/92106aef-9443-40be-8c8d-17edb92f37dc",
			Key:       "92106aef-9443-40be-8c8d-17edb92f37dc",
			Text:      "Yo, It's gonna be good life. This feeling you can't fight. It's gonna be good life. Good good life.",
			IsDeleted: false,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/92106aef-9443-40be-8c8d-17edb92f37dc",
		},
		&model.Post{
			Id:        config.COLLECTION.POST + "/8e941790-9b67-43e5-b680-40a5d96b2774",
			Key:       "8e941790-9b67-43e5-b680-40a5d96b2774",
			Text:      "spam, spam, spam",
			IsDeleted: false,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		},
		&model.Post{
			Id:        config.COLLECTION.POST + "/87fbe3db-5afd-46af-9a6f-b829a216e784",
			Key:       "87fbe3db-5afd-46af-9a6f-b829a216e784",
			Text:      "Delete me If you can",
			IsDeleted: true,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		},
		&model.Post{
			Id:        config.COLLECTION.POST + "/87fbe3db-5afd-46af-9a6f-b829a216e784",
			Key:       "87fbe3db-5afd-46af-9a6f-b829a216e784",
			Text:      "Lorem ipsum",
			IsDeleted: false,
			CreatedAt: "2023-01-20T19:31:11+08:00",
			CreatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
			UpdatedAt: "2023-01-20T19:31:11+08:00",
			UpdatedBy: "profiles/6cb2e861-502e-4327-8769-a2dbbb6e33e5",
		},
	},
}
