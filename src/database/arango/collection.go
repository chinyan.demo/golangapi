package arango

import (
	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"
	docHelper "ginapp/src/helper/document"
)

func SetupCollection() {
	db, err := GetDatabase()
	if err != nil {
		ColorLog.Red(err)
	}
	collections := docHelper.DocToMap(config.COLLECTION)
	for _, val := range collections {
		collection := val.(string)
		found, err := db.CollectionExists(nil, collection)
		if err != nil {
			ColorLog.Red(err)
		}
		if !found {
			_, err := db.CreateCollection(nil, collection, nil)
			if err != nil {
				ColorLog.Red(err)
			}
		}

	}
}
