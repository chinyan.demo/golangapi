package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	docHelper "ginapp/src/helper/document"
	"ginapp/src/model"
)

type InterestEdgeRepository struct {
	*baseRepo.BaseRepository
}

var InterestEdgeRepo = &InterestEdgeRepository{
	&baseRepo.BaseRepository{
		Collection: config.EDGE.INTERESTED,
	},
}

func (repo *InterestEdgeRepository) Recommendation(profileID string, friendlist any) ([]*model.Profile, error) {
	var friends []*model.Profile

	query := `
	FOR v IN 2..2
	ANY @profileID
	GRAPH interest_graph
		FILTER CONTAINS(v._id, "profiles/")
		FILTER v._id NOT IN @friendlist
	RETURN DISTINCT(v)
		`
	bindParams := map[string]any{
		"profileID":  profileID,
		"friendlist": friendlist,
	}
	mapData, err := repo.FindManyByQuery(query, bindParams)
	if err != nil {
		return friends, err
	}
	friends, err = docHelper.ParseDocs[*model.Profile](mapData)
	if err != nil {
		return friends, err
	}
	return friends, nil
}
