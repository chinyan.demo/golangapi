package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	docHelper "ginapp/src/helper/document"
	"ginapp/src/model"
)

type ProfileRepository struct {
	*baseRepo.BaseRepository
}

var ProfileRepo = &ProfileRepository{
	&baseRepo.BaseRepository{
		Collection: config.COLLECTION.PROFILE,
	},
}

func (repo *ProfileRepository) GetProfileByEmail(email string) ([]*model.Profile, error) {
	var profiles []*model.Profile
	query := "FOR p IN @@collection FILTER p.email == @email return p"
	bindVars := map[string]any{
		"@collection": config.COLLECTION.PROFILE,
		"email":       email,
	}

	mapProfile, err := repo.FindManyByQuery(query, bindVars)
	if err != nil {
		return profiles, err
	}
	profiles, err = docHelper.ParseDocs[*model.Profile](mapProfile)
	if err != nil {
		return profiles, err
	}

	return profiles, nil
}
