package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	docHelper "ginapp/src/helper/document"
	"ginapp/src/model"
)

type InterestRepository struct {
	*baseRepo.BaseRepository
}

var InterestRepo = &InterestRepository{
	&baseRepo.BaseRepository{
		Collection: config.COLLECTION.INTEREST,
	},
}

func (repo *InterestRepository) List() (any, error) {
	query := `FOR i IN @@collection RETURN i`
	bindVars := map[string]any{"@collection": config.COLLECTION.INTEREST}

	mapInterest, err := repo.FindManyByQuery(query, bindVars)
	if err != nil {
		return nil, err
	}
	Interests, err := docHelper.ParseDocs[model.Interest](mapInterest)
	if err != nil {
		return nil, err
	}

	return Interests, nil
}
