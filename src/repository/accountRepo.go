package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	docHelper "ginapp/src/helper/document"
	"ginapp/src/model"
)

type AccountRepository struct {
	*baseRepo.BaseRepository
}

var AccountRepo = &AccountRepository{
	&baseRepo.BaseRepository{
		Collection: config.COLLECTION.ACCOUNT,
	},
}

func (repo *AccountRepository) FetchByEmail(email string) ([]*model.Account, error) {
	var Accs []*model.Account
	query := `FOR acc IN @@collection FILTER acc.email == @email RETURN acc`
	bindVars := map[string]any{
		"@collection": config.COLLECTION.ACCOUNT,
		"email":       email,
	}
	mapAcc, err := repo.FindManyByQuery(query, bindVars)
	if err != nil {
		return Accs, err
	}
	Accs, err = docHelper.ParseDocs[*model.Account](mapAcc)
	if err != nil {
		return Accs, err
	}

	return Accs, nil

}
