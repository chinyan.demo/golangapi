package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	ColorLog "ginapp/src/helper/colorlog"
	docHelper "ginapp/src/helper/document"
)

type FriendRequestListModel struct {
	Name             string `json:"name"`
	ProfileID        string `json:"profileID"`
	FriendshipEdgeID string `json:"friendshipEdgeID"`
}
type FriendshipRepository struct {
	*baseRepo.BaseRepository
}

var FriendshipRepo = &FriendshipRepository{
	&baseRepo.BaseRepository{
		Collection: config.EDGE.FRIENDSHIP,
	},
}

func (repo *FriendshipRepository) GetFriendsID(profileID string) (any, error) {
	var friendsID any
	ColorLog.Yellow("profileID :  ", profileID)
	query := `
	LET friendsID = (
		FOR v,e IN 1..1
		ANY  @profileID 
		GRAPH friendship_graph 
		FILTER e.accepted == true
		RETURN v._id
	)
	RETURN {"ProfileID":friendsID}`
	bindsParam := map[string]any{
		"profileID": profileID,
	}
	mapFriendsID, err := repo.FindManyByQuery(query, bindsParam)
	if err != nil {
		return friendsID, err
	}
	ColorLog.Magenta("mapFriendsID :: ", mapFriendsID)
	friendsID = mapFriendsID[0]["ProfileID"]

	return friendsID, nil
}

func (repo *FriendshipRepository) FriendRequestList(profileID string) ([]*FriendRequestListModel, error) {
	var friendRequests []*FriendRequestListModel

	query := `
	FOR v,e IN 1..1
	INBOUND @profileID
	GRAPH friendship_graph 
	FILTER e.accepted == false
	COLLECT 
	    name = CONCAT(v.firstName," ",v.lastName),
	    profileID = v._id,
	    friendshipEdgeID = e._id
	RETURN { "name":name, "profileID":profileID ,"friendshipEdgeID":friendshipEdgeID}
	`
	bindsParam := map[string]any{
		"profileID": profileID,
	}
	mapFriendsID, err := repo.FindManyByQuery(query, bindsParam)
	if err != nil {
		return friendRequests, err
	}
	friendRequests, err = docHelper.ParseDocs[*FriendRequestListModel](mapFriendsID)
	if err != nil {
		return friendRequests, err
	}
	return friendRequests, nil
}
