package repository

import (
	"ginapp/src/config"
	baseRepo "ginapp/src/database"
	docHelper "ginapp/src/helper/document"
	"ginapp/src/model"
)

type PostRepository struct {
	*baseRepo.BaseRepository
}

var PostRepo = &PostRepository{
	&baseRepo.BaseRepository{
		Collection: config.COLLECTION.POST,
	},
}

func (repo *PostRepository) GetPosts(profileID string, friendsID any) ([]*model.Post, error) {
	var Posts []*model.Post

	query := `
	FOR post IN posts_view
	SEARCH post.createdBy == @profileID
	|| post.createdBy IN @friendsID
	filter post.isDeleted == false
	RETURN post`
	bindParams := map[string]any{
		"profileID": profileID,
		"friendsID": friendsID,
	}

	mapPosts, err := repo.FindManyByQuery(query, bindParams)
	if err != nil {
		return Posts, err
	}
	Posts, err = docHelper.ParseDocs[*model.Post](mapPosts)
	if err != nil {
		return Posts, err
	}

	return Posts, nil
}
