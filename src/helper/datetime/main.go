package datetime

import (
	t "time"
)

func GetIso8601() string {
	time := t.Now().Format(t.RFC3339)
	return time
}
