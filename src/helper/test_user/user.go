package userCredential

import (
	"bytes"
	"encoding/json"
	"ginapp/src/api/public"
	"ginapp/src/helper/cryptography"
	"io/ioutil"
	"net/http"
	"net/http/httptest"

	ColorLog "ginapp/src/helper/colorlog"

	"github.com/gin-gonic/gin"
)

var token string

func GetToken(App *gin.Engine) string {
	if len(token) == 0 {
		var loginResponse public.ResLogin

		payload := &public.LoginPayload{
			Email:    "stevenfoxx@gmail.com",
			Password: cryptography.Base64Encoding("somePassword@123!"),
		}
		buffer, _ := json.Marshal(payload)

		rec := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/login", bytes.NewBuffer(buffer))
		req.Header.Set("Content-Type", "application/json")
		App.ServeHTTP(rec, req)

		res := rec.Result()
		body, _ := ioutil.ReadAll(res.Body)
		jsonStr := string(body)
		if res.StatusCode == http.StatusOK {
			ColorLog.Green(jsonStr)
		} else {
			ColorLog.Red(jsonStr)
			return ""
		}

		err := json.Unmarshal([]byte(jsonStr), &loginResponse)
		if err != nil {
			ColorLog.Red(err.Error())
			return ""
		}
		token = loginResponse.Data.AccessToken
	}
	return token
}
