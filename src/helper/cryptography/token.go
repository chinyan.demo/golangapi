package cryptography

import (
	"strings"
	"time"

	"ginapp/src/config"
	ColorLog "ginapp/src/helper/colorlog"

	"github.com/golang-jwt/jwt"
)

func GenerateJWT(signature string, payload map[string]interface{}) (string, error) {
	var mySigningKey = []byte(signature)
	token := jwt.New(jwt.SigningMethodHS256)
	JwtData := token.Claims.(jwt.MapClaims)

	JwtData["authorized"] = true
	// JwtData["orgID"] = payload["orgID"]
	// JwtData["role"] = payload["role"]
	// JwtData["username"] = payload["username"]
	for key, value := range payload {
		// ColorLog.Green("{", key, ":", value, "}")
		JwtData[key] = value
	}
	if strings.ToLower(config.GetEnv().ENV) == "test" {
		JwtData["exp"] = time.Now().Add(time.Minute * 60 * 24 * 30 * 12 * 100).Unix() //expire 100 years later for test
	} else {
		JwtData["exp"] = time.Now().Add(time.Minute * 60 * 2).Unix()
	}

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		ColorLog.Red("Sign Token Error : " + err.Error())
		return "", err
	}
	return tokenString, nil
}
