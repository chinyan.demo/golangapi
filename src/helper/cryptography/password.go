package cryptography

import (
	"fmt"
	"math/rand"

	b64 "encoding/base64"

	"golang.org/x/crypto/bcrypt"
)

type (
	StoredPassword struct {
		HashedPassword string
		Salt           string
	}
)

func Base64Encoding(data string) string {
	return b64.StdEncoding.EncodeToString([]byte(data))
}

func RandomNumb() int {
	min := 1000000000000000
	max := 9999999999999999
	rand := rand.Intn(max-min+1) + min
	return rand
}

func HashPassword(password string) (error, string) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return err, ""
	}
	password = string(bytes)

	return nil, password

}

func HashPasswordWithSalt(password string) (string, string) {
	salt := fmt.Sprint(RandomNumb())
	bytes, err := bcrypt.GenerateFromPassword([]byte(password+salt), bcrypt.DefaultCost)
	if err != nil {
		return err.Error(), err.Error()
	}
	password = string(bytes)

	return salt, password

}
func CheckPasswordWithSalt(inputPassword string, HashedPassword string, Salt string) (bool, error) {
	salt := Salt
	authErr := bcrypt.CompareHashAndPassword([]byte(HashedPassword), []byte(inputPassword+salt))
	if authErr != nil {
		return false, authErr
	}
	return true, nil
}

func CheckPassword(inputPassword string, HashedPassword string) (bool, error) {
	authErr := bcrypt.CompareHashAndPassword([]byte(HashedPassword), []byte(inputPassword))
	if authErr != nil {
		return false, authErr
	}
	return true, nil
}
