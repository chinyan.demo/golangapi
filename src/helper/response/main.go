package Res

import (
	ColorLog "ginapp/src/helper/colorlog"
	"net/http"

	"github.com/gin-gonic/gin"
)

func Success[T any](msg string, data T, ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  200,
		"message": msg,
		"data":    data,
	})
}

func BadRequest(msg string, data error, ctx *gin.Context) {
	ColorLog.Magenta("Bad Request : ", data.Error())
	ColorLog.Magenta(" message : ", msg)
	ctx.JSON(http.StatusBadRequest, gin.H{
		"status":  400,
		"message": msg,
		"data":    data,
	})
}

func Unauthorized(msg string, data error, ctx *gin.Context) {
	ColorLog.Magenta("Unauthorized : ", data.Error())
	ColorLog.Magenta(" message :", msg)
	ctx.JSON(http.StatusUnauthorized, gin.H{
		"status":  401,
		"message": msg,
		"data":    data,
	})
}

func InternalServerError(msg string, data error, ctx *gin.Context) {
	ColorLog.Red("Internal Server Error : ", data.Error())
	ColorLog.Red("\n message : ", msg)
	ctx.JSON(http.StatusInternalServerError, gin.H{
		"status":  500,
		"message": msg,
		"data":    data,
	})

}
