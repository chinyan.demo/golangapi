package config

type collection struct {
	//Collection
	PROFILE           string
	ACCOUNT           string
	INTEREST          string
	INTEREST_CATEGORY string
	POST              string
}

type edge struct {
	//EDGE Collection
	FRIENDSHIP        string
	INTERESTED        string
	INTEREST_CATEGORY string
}

var COLLECTION = &collection{
	PROFILE:           "profiles",
	ACCOUNT:           "accounts",
	INTEREST:          "interests",
	INTEREST_CATEGORY: "interest_categories",
	POST:              "posts",
}

var EDGE = &edge{
	FRIENDSHIP:        "friendship_edge",
	INTERESTED:        "interested_edge",
	INTEREST_CATEGORY: "interest_category_edge",
}
