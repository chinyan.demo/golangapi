package config

import (
	ColorLog "ginapp/src/helper/colorlog"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/viper"
)

type Configuration struct {
	HOST string `mapstructure:"HOST"`
	PORT string `mapstructure:"PORT"`
	ENV  string `mapstructure:"ENV"`

	ARANGODB_PROTOCOL string `mapstructure:"ARANGODB_PROTOCOL"`
	ARANGODB_HOST     string `mapstructure:"ARANGODB_HOST"`
	ARANGODB_PORT     string `mapstructure:"ARANGODB_PORT"`
	ARANGODB_USER     string `mapstructure:"ARANGODB_USER"`
	ARANGODB_PASSWORD string `mapstructure:"ARANGODB_PASSWORD"`
	ARANGODB_DBNAME   string `mapstructure:"ARANGODB_DBNAME"`

	JWT_SIGNATURE string `mapstructure:"JWT_SIGNATURE"`
}

//Instance
var env Configuration

var (
	workingDir, _ = os.Getwd()
	envPath       = filepath.Dir(workingDir) + "/" + filepath.Base(workingDir) //+ "/.env"
	conf, _       = getConfig(envPath, "dev")
	testConf, _   = getConfig(envPath, "test")
)

func getConfig(path, env_name string) (*Configuration, error) {
	path = strings.Split(path, "/src")[0] // This is for test since it start from api dir
	// ColorLog.Cyan("path : ", path)
	config := &Configuration{}
	viper.AddConfigPath(path)
	viper.SetConfigName(env_name)
	viper.SetConfigType("env")
	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		ColorLog.Red(err)
		return nil, err
	}

	err = viper.Unmarshal(config)
	if err != nil {
		ColorLog.Red(err)
		return nil, err
	}
	// ColorLog.Magenta("Config : ", config)
	return config, nil

}

func GetEnv() Configuration {
	env = *conf
	// ColorLog.Magenta("APP_ENV : ", os.Getenv("APP_ENV"))
	if os.Getenv("APP_ENV") == "test" {
		env = *testConf
	}
	return env
}

func GetTestEnv() Configuration {
	return *testConf
}
