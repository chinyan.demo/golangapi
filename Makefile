arango:
	docker-compose up -d

start:
	go run main.go

dev:
	air

build:
	go build

test:
	go clean -testcache
	APP_ENV=test go test -v ./src/api/...